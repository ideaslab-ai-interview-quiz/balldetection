# Quiz description

## Introduction
- Design and train a golf ball detection model and then combine with a signal processor such as Kalman filter to complete golf ball tracking task in videos.

## Details
- The quiz can be separated into 2 stages.
    1. Design and train a golf ball detection model with cropped images
    2. Design a pipeline to let the model can be apply to real world case not just on cropped images. 
- The first stage
    1. It's more preferable to build heatmap model like unet or deeplab with changable encoder structure.
        - Hint: you can use the package 'timm' to create encoder
        - Hint: https://github.com/qubvel/segmentation_models.pytorch/issues/373
        - Reference: https://github.com/huggingface/pytorch-image-models
        - You can choose BCEWithLogitsLoss for your loss
    2. If you don't have plenty of time, you can try with YOLOv8
        - Reference: https://docs.ultralytics.com/quickstart/#use-ultralytics-with-python
    3. For the training sources, I might need you to use google colab or kaggle for the GPUs. 
    4. Final visualization:
        - Heatmap based model: overlap the prediction heatmap and it's input together. 
        - Like the images: https://drive.google.com/drive/folders/1C1D3MXAlynfY3srf9S4khA4qqhpWIseA?usp=drive_link
        - YOLO model: draw a circle on the ball with the highest confidence level.
        - Please use the testing images in training set for the visualization.
- The second stage (optional)
    1. You need to design a regression method for the heatmap to get the xy of the ball. (YOLO can skip this step)
    2. Build a signal processor and combine with your model to finish the pipeline like the demo video in link below. (the searching window size can be constant)
        - https://drive.google.com/drive/folders/1m6M7fJeM5RXSwlMGsyy_Pp2U_RopFHL2?usp=drive_link
        - Hint: you can use Kalman filter
        - Reference: 
            - (Mandarin) https://medium.com/vswe/kalman-filter-987a17bb8f07
            - (English) https://www.cs.unc.edu/~welch/media/pdf/kalman_intro.pdf

## Dataset
- https://drive.google.com/drive/folders/1_m9DrratNvHFBP0m39YSlmXYCfthq3xq?usp=sharing
- This dataset is not very clean. You might need to do some data exploring.
- If you don't have time for this, here is the documentation: 
    - online: https://www.notion.so/Golf-ball-3a23d12d5ff746e596c9d5bf4d65ab37?pvs=4 (You need to be invited)
    - offline: check out the md file in dataset/document